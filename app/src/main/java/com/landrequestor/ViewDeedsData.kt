package com.landrequestor

import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.denzcoskun.imageslider.ImageSlider
import com.denzcoskun.imageslider.models.SlideModel
import com.esri.arcgisruntime.ArcGISRuntimeEnvironment
import com.esri.arcgisruntime.geometry.Point
import com.esri.arcgisruntime.geometry.SpatialReferences
import com.esri.arcgisruntime.mapping.ArcGISMap
import com.esri.arcgisruntime.mapping.Basemap
import com.esri.arcgisruntime.mapping.view.Graphic
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay
import com.esri.arcgisruntime.symbology.PictureMarkerSymbol
import com.esri.arcgisruntime.symbology.SimpleMarkerSymbol
import com.landrequestor.api.RetrofitClient
import com.landrequestor.model.CancelResponse
import com.landrequestor.model.CancelRequest
import com.landrequestor.model.Singleton
import com.landrequestor.model.deed.All_DeedsItem
import kotlinx.android.synthetic.main.activity_view_deeds_data.*
import kotlinx.android.synthetic.main.activity_view_deeds_data.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ViewDeedsData : AppCompatActivity() {
    var detail: All_DeedsItem? = null
    lateinit var pointGraphic: Graphic
    lateinit var point: Point
    //Mark the location
    private var mGraphicsOverlay: GraphicsOverlay? = null
    private var mPointSymbol: SimpleMarkerSymbol? = null
    var lat:Double?=null
    var lon:Double?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_deeds_data)


        val DeedID: Int = intent.getIntExtra("DeedID",0)

        d("ViewDeedPage", "deed_id : $DeedID")
        RetrofitClient.instance.getDeedbyID(DeedID).enqueue(object : Callback<All_DeedsItem> {
            override fun onFailure(call: Call<All_DeedsItem>, t: Throwable) {
                d("ViewDeedPage", "Call Request Data Failed!")
            }
            override fun onResponse(call: Call<All_DeedsItem>, response: Response<All_DeedsItem>) {

                detail = response.body()!!
                d("ViewDeedPage", "detail : ${detail}")
                //Set data
                lat=detail?.latitude
                lon=detail?.longitude


                var waiting = Singleton.config?.resources?.status?.find { it.name.toString().toUpperCase() == "WAITING" }?.statusId
                var status_name = Singleton.config?.resources?.status?.find { it.statusId == detail?.statusId }?.name
                if (detail?.statusId==waiting){ view_btn_cancel.visibility=View.VISIBLE }

                if (detail?.deedName!=null){view_txt_deedname.text=detail?.deedName}
                if (detail?.address!=null){view_txt_address.text=detail?.address}
                if (detail?.statusId!=null){view_status_txt.text=status_name}

                if (detail?.deedNumber!=null){view_txt_deednumber.text=detail?.deedNumber}
                if (detail?.deedType?.name!=null){view_txt_deedtype.text=detail?.deedType?.name}
                if (detail?.createdDate!=null){view_txt_createdate.text=detail?.createdDate}
                if (detail?.employee?.firstName!=null){view_txt_employeename.text=detail?.employee?.firstName+" "+detail?.employee?.lastName}
                if (detail?.nearbyLandmark!=null){view_txt_landmark.text=detail?.nearbyLandmark}
                if(detail?.area!=null){
                    view_txt_area.text=String.format("%.1f", detail?.area)+" SQM"
                    animation.visibility= View.VISIBLE
                    view_btn_cancel.visibility=View.GONE
                }


                if(detail?.latitude!=null && detail?.longitude!=null){
                    setupMap(10,detail?.latitude,detail?.longitude)
                    val mDrawable = ContextCompat.getDrawable(applicationContext!!, R.drawable.pin) as BitmapDrawable
                    createPointGraphics(detail?.latitude,detail?.longitude,mDrawable)
                }



                //IMG Slide
                if(detail?.imagePath!=null) {
                    var imgList = detail?.imagePath //List String
                    d("ViewDeedPage", "imgList : $imgList")
                            val imageList = ArrayList<SlideModel>()
                            imgList?.forEach {
                                // imageList.add(SlideModel("String Url" or R.drawable, "title", true) ; true = centerCrop scaleType for image
                                imageList.add(SlideModel("http://land-surveyor-dev.eba-atp2ae9g.ap-southeast-1.elasticbeanstalk.com$it",true))
                            }
                            val imageSlider = findViewById<ImageSlider>(R.id.image_slider)
                            imageSlider.setImageList(imageList)
                }


                view_btn_cancel.setOnClickListener{
                    val builder = AlertDialog.Builder(this@ViewDeedsData)
                    builder.setTitle("Confiirm?")
                    builder.setMessage("Are you sure to cancel this request?")
                    builder.setPositiveButton("YES") { dialog, which ->
                        val status = CancelRequest(status_id = 5)
                        RetrofitClient.instance.cancleRequest(DeedID, status)
                            .enqueue(object : Callback<CancelResponse> {
                                override fun onFailure(call: Call<CancelResponse>, t: Throwable) {
                                    Toast.makeText(this@ViewDeedsData,"Cancel Failed!", Toast.LENGTH_SHORT).show()
                                }

                                override fun onResponse(call: Call<CancelResponse>, response: Response<CancelResponse>) {
                                    Toast.makeText(this@ViewDeedsData,"Cancel Success!", Toast.LENGTH_SHORT).show()
                                    val i = Intent(this@ViewDeedsData, MainActivity::class.java)
                                    i.putExtra("checkFrg",true)
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    startActivity(i)
                                    finish()
                                }
                            })
                    }
                    builder.setNegativeButton("No"){dialog,which ->
                        Toast.makeText(this@ViewDeedsData,"You aren't agree.", Toast.LENGTH_SHORT).show()
                    }
                    val dialog: AlertDialog = builder.create()
                    dialog.show()
                }
            }
        })

        btn_zoom.setOnClickListener {

            val i = Intent(this@ViewDeedsData, ViewLocation::class.java)
            i.putExtra("lat",lat)
            i.putExtra("lon",lon)
            i.putExtra("checkFrg",true)
            startActivity(i)
        }
    }
    fun setupMap(levelmap : Int=1, Lat : Double?=13.702839, Lon : Double?=100.543718) {
        ArcGISRuntimeEnvironment.setLicense(resources.getString(R.string.arcgis_license_key))
        if (show_map != null) {
            if (mGraphicsOverlay?.graphics != null){
                mGraphicsOverlay?.graphics?.clear()
            }

            val basemapType = Basemap.Type.OPEN_STREET_MAP
            show_map.isAttributionTextVisible = false

            show_map.map = ArcGISMap(basemapType, Lat!!, Lon!!, levelmap)

            //Add layer mark location
            mGraphicsOverlay = GraphicsOverlay()
            show_map.graphicsOverlays.add(mGraphicsOverlay)

        }
    }
    fun createPointGraphics(Lat : Double?=null, Lon : Double?=null, mSymbol: BitmapDrawable?) {
        if (Lat != null && Lon != null) {
            mGraphicsOverlay?.graphics?.clear()

            val pinSymbol: PictureMarkerSymbol = PictureMarkerSymbol.createAsync(mSymbol).get()
            pinSymbol.height = 30F
            pinSymbol.width = 30F
            pinSymbol.loadAsync()

            mPointSymbol = SimpleMarkerSymbol(SimpleMarkerSymbol.Style.X, -0xffc757, 20F)

            point = Point(Lon!!, Lat!!, SpatialReferences.getWgs84())
            pointGraphic = Graphic(point, pinSymbol)
            mGraphicsOverlay?.graphics?.add(pointGraphic)
        }
    }

}
