package com.landrequestor

import android.R
import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Handler
import android.util.Log.d
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageButton
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.esri.arcgisruntime.geometry.GeometryEngine
import com.esri.arcgisruntime.geometry.Point
import com.esri.arcgisruntime.geometry.SpatialReferences
import com.esri.arcgisruntime.mapping.ArcGISMap
import com.esri.arcgisruntime.mapping.Basemap
import com.esri.arcgisruntime.mapping.view.Graphic
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay
import com.esri.arcgisruntime.mapping.view.MapView
import com.esri.arcgisruntime.symbology.PictureMarkerSymbol
import com.esri.arcgisruntime.symbology.SimpleMarkerSymbol
import kotlinx.android.synthetic.main.progress_dialog.view.*
//
//
fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}
//lateinit var pointGraphic: Graphic
//lateinit var point: Point
////Mark the location
//private var mGraphicsOverlay: GraphicsOverlay? = null
//private var mPointSymbol: SimpleMarkerSymbol? = null
////Current location
//private var cGraphicsOverlay: GraphicsOverlay? = null
//private var cPointSymbol: SimpleMarkerSymbol? = null
//
//var lat : Double?=null
//var lon : Double?=null
//
////Creat ArcGIS Map
//fun setupMap(map: MapView?, levelmap : Int=1, Lat : Double?=13.702839, Lon : Double?=100.543718) {
//
//    if (map != null) {
//        if (mGraphicsOverlay?.graphics != null){
//            mGraphicsOverlay?.graphics?.clear()
//        }
//
//        val basemapType = Basemap.Type.OPEN_STREET_MAP
//        map.isAttributionTextVisible = false
//
//        map.map = ArcGISMap(basemapType, Lat!!, Lon!!, levelmap)
//
//        //Add layer mark location
//        mGraphicsOverlay = GraphicsOverlay()
//        map.graphicsOverlays.add(mGraphicsOverlay)
//
//        //Add layer current location
//        cGraphicsOverlay = GraphicsOverlay()
//        map.graphicsOverlays.add(cGraphicsOverlay)
//
//    }
//}
//
//fun getlocation(btn: ImageButton,map : MapView){
//    val screenX=(btn.left+btn.right)/2
//    val screenY=btn.bottom
//    val screenPoint = android.graphics.Point(screenX,screenY)
//    val mapPoint = map.screenToLocation(screenPoint)
//    val wgs84Point = GeometryEngine.project(mapPoint, SpatialReferences.getWgs84()) as Point
//    lat= wgs84Point.y
//    lon= wgs84Point.x
//    d("testPoint", "Latitude: " + String.format("%.6f", wgs84Point.y) + ", Lontitude: " + String.format("%.6f", wgs84Point.x))
//}
//
//fun createPointGraphics(map : MapView, Lat : Double?= lat, Lon : Double?=lon, mSymbol:BitmapDrawable?){
//    if (Lat!=null && Lon!=null){
//        mGraphicsOverlay?.graphics?.clear()
//
//        val pinSymbol: PictureMarkerSymbol = PictureMarkerSymbol.createAsync(mSymbol).get()
//        pinSymbol.height = 30F
//        pinSymbol.width = 30F
//        pinSymbol.loadAsync()
//
////    mPointSymbol = SimpleMarkerSymbol(SimpleMarkerSymbol.Style.X, -0xffc757, 20F)
//
//        point = Point(Lon!!, Lat!!, SpatialReferences.getWgs84())
//        pointGraphic = Graphic(point, pinSymbol)
//        mGraphicsOverlay?.graphics?.add(pointGraphic)
//    }
//
//}
//
//fun currentloactionGraphics(map : MapView, Lat : Double?= lat, Lon : Double?=lon, cSymbol:BitmapDrawable?){
//    if (Lat!=null && Lon!=null) {
//        cGraphicsOverlay?.graphics?.clear()
//
//        val currentSymbol: PictureMarkerSymbol = PictureMarkerSymbol.createAsync(cSymbol).get()
//        currentSymbol.height = 30F
//        currentSymbol.width = 30F
//        currentSymbol.loadAsync()
//
////    cPointSymbol = SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, -0x72ff59, 18F)
//
//        point = Point(Lon!!, Lat!!, SpatialReferences.getWgs84())
//        pointGraphic = Graphic(point, currentSymbol)
//        cGraphicsOverlay?.graphics?.add(pointGraphic)
//    }
//}
//

//
//
////override fun onPause() {
////    if (MapView != null) {
////        MapView.pause()
////    }
////    super.onPause()
////}
////
////override fun onResume() {
////    super.onResume()
////    if (MapView != null) {
////        MapView.resume()
////    }
////}
////
////override fun onDestroy() {
////    if (MapView != null) {
////        MapView.dispose()
////    }
////    super.onDestroy()
////}
