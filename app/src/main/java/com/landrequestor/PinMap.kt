package com.landrequestor

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.BitmapDrawable
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.esri.arcgisruntime.ArcGISRuntimeEnvironment
import com.esri.arcgisruntime.geometry.GeometryEngine
import com.esri.arcgisruntime.geometry.Point
import com.esri.arcgisruntime.geometry.SpatialReferences
import com.esri.arcgisruntime.mapping.ArcGISMap
import com.esri.arcgisruntime.mapping.Basemap
import com.esri.arcgisruntime.mapping.view.Graphic
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay
import com.esri.arcgisruntime.symbology.PictureMarkerSymbol
import com.esri.arcgisruntime.symbology.SimpleMarkerSymbol
import com.google.android.gms.location.*
import com.landrequestor.api.GeoSearch
import com.landrequestor.fragment.AddFragment
import com.landrequestor.model.CheckFragment
import com.landrequestor.model.LatLon
import com.landrequestor.model.search.*
import kotlinx.android.synthetic.main.activity_pin_map.*
import kotlinx.android.synthetic.main.search_item.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class PinMap : AppCompatActivity() {
    var Key="49290361a331ef747417ff9a8a27b5f4524321da3c27b5b08ef8bb37d9c3de8d4354031e0ed44280"
    var maxResult = 7

    var place_list = arrayListOf<String>()
    var place_id = arrayListOf<String>()

    var currentLat:Double?=null
    var currentLon:Double?=null
    val PERMISSION_ID = 42
    lateinit var mFusedLocationClient: FusedLocationProviderClient
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    var lat:Double?=null
    var lon:Double?=null
    lateinit var pointGraphic: Graphic
    lateinit var point: Point
    //Mark the location
    private var mGraphicsOverlay: GraphicsOverlay? = null
    private var mPointSymbol: SimpleMarkerSymbol? = null
    //Current location
    private var cGraphicsOverlay: GraphicsOverlay? = null
    private var cPointSymbol: SimpleMarkerSymbol? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pin_map)

        setupMap(5)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        getLastLocation()//setup map + current location

        current_btn.setOnClickListener {
            getLastLocation()
        }


        search_txt.doAfterTextChanged {
            if(search_txt.text.trim().toString()==""){ autocomplete_list.visibility=View.GONE }
            else{getAutocomplete()}
        }

        pin_btn.setOnClickListener {
            getlocation()
            val mDrawable = ContextCompat.getDrawable(this!!, R.drawable.mark_location) as BitmapDrawable
            createPointGraphics(lat,lon,mDrawable)
            btn_confirm.visibility=View.INVISIBLE
            btn_confirm.isEnabled=true
            btn_confirm.performClick()
//            btn_confirm.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(R.color.darkBlue)))
        }

        btn_confirm.setOnClickListener {
            val newBundle = Bundle()
            newBundle.putDouble("latArg", lat!!)
            newBundle.putDouble("lonArg", lon!!)
            val objects = AddFragment()
            objects.arguments = newBundle
            LatLon.latitude=lat
            LatLon.longitude=lon
            CheckFragment.frg="add"

            val i = Intent(this, MainActivity::class.java)
//            i.putExtra("checkFrg",true)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(i)
            finish()
        }
    }

    fun setupMap(levelmap : Int=1, Lat : Double?=13.702839, Lon : Double?=100.543718) {
        ArcGISRuntimeEnvironment.setLicense(resources.getString(R.string.arcgis_license_key))
        if (mapView != null) {
            if (mGraphicsOverlay?.graphics != null){
                mGraphicsOverlay?.graphics?.clear()
            }

            val basemapType = Basemap.Type.OPEN_STREET_MAP
            mapView.isAttributionTextVisible = false

            mapView.map = ArcGISMap(basemapType, Lat!!, Lon!!, levelmap)

            //Add layer mark location
            mGraphicsOverlay = GraphicsOverlay()
            mapView.graphicsOverlays.add(mGraphicsOverlay)

            //Add layer current location
            cGraphicsOverlay = GraphicsOverlay()
            mapView.graphicsOverlays.add(cGraphicsOverlay)

        }
    }
    fun createPointGraphics(Lat : Double?= lat, Lon : Double?=lon, mSymbol:BitmapDrawable?) {
        if (Lat != null && Lon != null) {
            mGraphicsOverlay?.graphics?.clear()

            val pinSymbol: PictureMarkerSymbol = PictureMarkerSymbol.createAsync(mSymbol).get()
            pinSymbol.height = 30F
            pinSymbol.width = 30F
            pinSymbol.loadAsync()

            mPointSymbol = SimpleMarkerSymbol(SimpleMarkerSymbol.Style.X, -0xffc757, 20F)

            point = Point(Lon!!, Lat!!, SpatialReferences.getWgs84())
            pointGraphic = Graphic(point, mPointSymbol)
            mGraphicsOverlay?.graphics?.add(pointGraphic)
        }
    }

    fun currentloactionGraphics(Lat : Double?= lat, Lon : Double?=lon, cSymbol:BitmapDrawable?) {
        if (Lat != null && Lon != null) {
            cGraphicsOverlay?.graphics?.clear()

            val currentSymbol: PictureMarkerSymbol = PictureMarkerSymbol.createAsync(cSymbol).get()
            currentSymbol.height = 30F
            currentSymbol.width = 30F
            currentSymbol.loadAsync()

//    cPointSymbol = SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, -0x72ff59, 18F)

            point = Point(Lon!!, Lat!!, SpatialReferences.getWgs84())
            pointGraphic = Graphic(point, currentSymbol)
            cGraphicsOverlay?.graphics?.add(pointGraphic)
        }
    }

    fun getlocation() {
        val screenX = (pin_layout.left + pin_layout.right) / 2
        val screenY = (pin_layout.top+pin_layout.bottom)/2
        val screenPoint = android.graphics.Point(screenX, screenY)
        val mapPoint = mapView.screenToLocation(screenPoint)
        val wgs84Point = GeometryEngine.project(mapPoint, SpatialReferences.getWgs84()) as Point
        lat = wgs84Point.y
        lon = wgs84Point.x
        d("testPoint", "Latitude: " + String.format("%.6f", wgs84Point.y) + ", Lontitude: " + String.format("%.6f", wgs84Point.x))
    }


    fun getAutocomplete(){
        place_list.clear()
        place_id.clear()
        autocomplete_list.visibility=View.VISIBLE
        val Keyword=search_txt.text.toString()
        d("PinMapPage","Keyword : $Keyword")

        GeoSearch.instance.auto_complete(AutocompleteRequest(Keyword, Key, maxResult)).enqueue(object : Callback<Autocomplete> {
            override fun onFailure(call: Call<Autocomplete>, t: Throwable) {
                d("PinMapPage", "Get autocomplete fail! ${t.message}")
            }

            override fun onResponse(call: Call<Autocomplete>, response: Response<Autocomplete>) {
                val data = response.body()?.data
                d("PinMapPage", "Type data : ${data!!::class.qualifiedName}, data : ${data}")
//                                autoList =data!!.toList()
//                                d("PinMapPage", "Type : ${autoList!!::class.qualifiedName} ,Data Autocomplete : ${autoList}")

                data.forEach {
                    place_list.add(it.FormattedAddress!!)
                    place_id.add(it.LocationID!!)
//                                        d("PinMapPage","it :${it.FormattedAddress}")
                }
//                                d("PinMapPage","place_list Type : ${place_list!!::class.qualifiedName} place_list : $place_list, place_list.size= ${place_list.size}")
                autocomplete_list.adapter = CustomAdapter()
                autocomplete_list.layoutManager= LinearLayoutManager(this@PinMap)
            }
        })
    }

    //Show suggestion place
    inner class CustomAdapter : RecyclerView.Adapter<CustomHolder>() {
        override fun onCreateViewHolder(parentView: ViewGroup, option: Int): CustomHolder {
//            d("HomePage","place_list : $place_list, place_list.size= ${place_list.size}")
            val view= LayoutInflater.from(this@PinMap).inflate(R.layout.search_item,parentView,false)
            return CustomHolder(view)
        }

        override fun getItemCount(): Int { return place_list.size }

        override fun onBindViewHolder(holder: CustomHolder, position: Int) {
            holder.address.text=place_list[position]
            holder.row.setOnClickListener {
                var locationID=place_id[position]
                d("PinMapPage", "locationID : $locationID")
                getLocationDetail(locationID)
                search_txt.setText("")
                search_txt.hideKeyboard()
//                Toast.makeText(this@PinMap, "ID : $locationID \n \n Name : ${holder.address.text}", Toast.LENGTH_LONG).show()
                autocomplete_list.visibility=View.GONE
            }
        }
    }
    inner  class CustomHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var row=itemView.row
        var address=itemView.txt_address
    }
    //get location search : lat lon
    fun getLocationDetail(id:String){
        GeoSearch.instance.get_locationDetails(LocationDetailsRequest(id,Key)).enqueue(object : Callback<LocationDetails> {
            override fun onFailure(call: Call<LocationDetails>, t: Throwable) {
                d("PinMapPage","getLocationDetail ERROR")
            }
            override fun onResponse(call: Call<LocationDetails>, response: Response<LocationDetails>) {
                var detail=response.body()?.data
                var latlon=detail?.LATLON?.split(", ")
//                d("PinMapPage","lat : ${latlon!![0]}, lon : ${latlon!![1]}")
                setupMap(17,latlon!![0].toDouble(),latlon!![1].toDouble())
//                val mDrawable = ContextCompat.getDrawable(applicationContext, R.drawable.mark_location) as BitmapDrawable
//                createPointGraphics(latlon!![0].toDouble(),latlon!![1].toDouble(),mDrawable)
//                d("PinMapPage","getLocationDetail ::: lat: ${detail!![0].toDouble()} lon: ${detail!![1].toDouble()}")
            }

        })
    }





    //Current Location
    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                    var location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                        currentLat= location.latitude
                        currentLon= location.longitude

                        setupMap(17,currentLat!!,currentLon!!)
                        if(currentLat!=null && currentLon !=null){

                            val cmDrawable = ContextCompat.getDrawable(this, R.drawable.current_location) as BitmapDrawable
                            currentloactionGraphics(currentLat!!,currentLon!!,cmDrawable)
                        }
                    }
                }
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        var mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient!!.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
//            var mLastLocation: Location = locationResult.lastLocation
//            latTextView.text = mLastLocation.latitude.toString()
//            lonTextView.text = mLastLocation.longitude.toString()
        }
    }

    private fun isLocationEnabled(): Boolean {
        var locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
            PERMISSION_ID
        )
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLastLocation()
            }
        }
    }


    //Map
    override fun onPause() {
        if (mapView != null) {
            mapView.pause()
        }
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        if (mapView != null) {
            mapView.resume()
        }
    }

    override fun onDestroy() {
        if (mapView != null) {
            mapView.dispose()
        }
        super.onDestroy()
    }

}




