package com.landrequestor

import android.graphics.drawable.BitmapDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import androidx.core.content.ContextCompat
import com.esri.arcgisruntime.ArcGISRuntimeEnvironment
import com.esri.arcgisruntime.geometry.Point
import com.esri.arcgisruntime.geometry.SpatialReferences
import com.esri.arcgisruntime.mapping.ArcGISMap
import com.esri.arcgisruntime.mapping.Basemap
import com.esri.arcgisruntime.mapping.view.Graphic
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay
import com.esri.arcgisruntime.symbology.PictureMarkerSymbol
import com.esri.arcgisruntime.symbology.SimpleMarkerSymbol
import kotlinx.android.synthetic.main.activity_pin_map.*

class ViewLocation : AppCompatActivity() {

    //Mark the location
    private var mGraphicsOverlay: GraphicsOverlay? = null
    lateinit var pointGraphic: Graphic
    lateinit var point: Point

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_location)


        var lat= intent.extras.getDouble("lat")
        var lon= intent.extras.getDouble("lon")
        d("ViewLocation","$lat $lon")
        setupMap(10,lat,lon)
        val mDrawable = ContextCompat.getDrawable(applicationContext!!, R.drawable.pin) as BitmapDrawable
        createPointGraphics(lat,lon,mDrawable)


    }

    fun setupMap(levelmap : Int=1, Lat : Double?=13.702839, Lon : Double?=100.543718) {
        ArcGISRuntimeEnvironment.setLicense(resources.getString(R.string.arcgis_license_key))

            val basemapType = Basemap.Type.OPEN_STREET_MAP
            mapView.isAttributionTextVisible = false

            mapView.map = ArcGISMap(basemapType, Lat!!, Lon!!, levelmap)

            //Add layer mark location
            mGraphicsOverlay = GraphicsOverlay()
            mapView.graphicsOverlays.add(mGraphicsOverlay)


    }
    fun createPointGraphics(Lat : Double?= 0.0, Lon : Double?= 0.0, mSymbol: BitmapDrawable?) {
        if (Lat != null && Lon != null) {
            mGraphicsOverlay?.graphics?.clear()

            val pinSymbol: PictureMarkerSymbol = PictureMarkerSymbol.createAsync(mSymbol).get()
            pinSymbol.height = 30F
            pinSymbol.width = 30F
            pinSymbol.loadAsync()

            var mPointSymbol: SimpleMarkerSymbol? = null
            mPointSymbol = SimpleMarkerSymbol(SimpleMarkerSymbol.Style.X, -0xffc757, 20F)

            point = Point(Lon, Lat, SpatialReferences.getWgs84())
            pointGraphic = Graphic(point, pinSymbol)
            mGraphicsOverlay?.graphics?.add(pointGraphic)
        }
    }

}
