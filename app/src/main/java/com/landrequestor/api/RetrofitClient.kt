package com.landrequestor.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {
    private  const val  BASE_URL="http://land-surveyor-dev.eba-atp2ae9g.ap-southeast-1.elasticbeanstalk.com/api/v1/"
    val instance: ApiService by lazy{
        val retrofit= Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        retrofit.create(ApiService::class.java)
    }
}

object GeoSearch {
    private  const val  BASE_URL="https://geosearch.cdg.co.th/"
    val instance: SearchService by lazy{
        val retrofit= Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        retrofit.create(SearchService::class.java)
    }
}