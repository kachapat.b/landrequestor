package com.landrequestor.api

import com.landrequestor.model.search.*
import retrofit2.Call
import retrofit2.http.*


interface SearchService {

    @Headers("referer: *.cdg.co.th")
    @POST("g/search/autocomplete")
    fun auto_complete(
        @Body request_body: AutocompleteRequest
    ): Call<Autocomplete>

    @Headers("referer: *.cdg.co.th")
    @POST("g/search/details")
    fun get_locationDetails(
        @Body request_body: LocationDetailsRequest
    ):Call<LocationDetails>


}