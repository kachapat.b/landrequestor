package com.landrequestor.api

import com.landrequestor.model.*
import com.landrequestor.model.CancelRequest
import com.landrequestor.model.configs.ConfigsResponse
import com.landrequestor.model.deed.All_Deeds
import com.landrequestor.model.deed.All_DeedsItem
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


public interface ApiService {

    @Headers("Content-Type: application/json")
    @POST("login")
    fun userLogin(
        @Body request_body: LoginRequest
        //@Field("username") username:String,
        //@Field("password") password:String
    ):Call<LoginResponse>


    @Multipart
    @POST("deeds")
    fun addRequest(
        @Part("customer_id") customer_id: RequestBody,
        @Part("deed_name") deed_name: RequestBody,
        @Part("address") address: RequestBody,
        @Part("nearby_landmark") landmark: RequestBody,
        @Part("latitude") latitude: RequestBody,
        @Part("longitude") longitude: RequestBody,
        @Part image: ArrayList<MultipartBody.Part>
    ):Call<ResponseBody>



    @GET("customers")
    fun  getUserdata(
        @Query("user_id")user_ID:Int?
    ):Call<User>


    @Headers("Content-Type: application/json")
    @PATCH("deeds/{id}")
    fun cancleRequest(
        @Path("id")id:Int,
        @Body cancleStatus: CancelRequest
    ):Call<CancelResponse>

    @GET("deeds")
    fun getAllDeed(
        @Query("customer_id")customer_ID:Int?,
        @Query("status_id")status:String,
        @Query("sort")sort:String?="status_id,updated_date"
    ):Call<All_Deeds>


    @GET("deeds/{id}")
    fun getDeedbyID(
        @Path("id")id:Int?
    ): Call<All_DeedsItem>

    @GET("configs")
    fun getConfigs(
        @Query("user_id")user_id:Int?
    ): Call<ConfigsResponse>


}