package com.landrequestor

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import kotlinx.android.synthetic.main.activity_login.*
import android.widget.Toast
import com.landrequestor.api.RetrofitClient
import com.landrequestor.model.LoginRequest
import com.landrequestor.model.LoginResponse
import com.landrequestor.model.Singleton
import com.landrequestor.model.configs.ConfigsResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Login : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        //Click Log in Button
        btn_login.setOnClickListener {
            val username=editText_username.text.toString().trim()
            val password=editText_password.text.toString().trim()

            //Check username and password isn't null
            if(username.isEmpty()){
                editText_username.error="username required"
                editText_username.requestFocus()
                return@setOnClickListener
            }
            if(password.isEmpty()){
                Text_password.error="password required"
                editText_password.requestFocus()
            }


            d("LoginPage", "username : $username , $password")

            //Post username , password
            val requestLogin= LoginRequest(username,password,"customer")

            RetrofitClient.instance.userLogin(requestLogin)
                    .enqueue(object: Callback<LoginResponse> {
                        override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                            Toast.makeText(applicationContext,"Cannot connect to data server!",Toast.LENGTH_SHORT).show()
                            d("LoginPage", "cannot connect to data server ${t.message}")
                        }

                        override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                            if(response.body()?.user_id==null){
                                Toast.makeText(applicationContext,"Invalid username or password",Toast.LENGTH_SHORT).show()
                                d("LoginPage", "Invalid Username or Password")

                            }else{
                                Toast.makeText(applicationContext,"Login success!",Toast.LENGTH_SHORT).show()
                                //Call function callUserdata
                                callUserdata(response.body()!!.user_id)
                                d("LoginPage", "Login success, user id: ${response.body()?.user_id}")
                            }
                        }
                    })
        }
    }


    private fun callUserdata(userId: Int?) {
        d("LoginPage", "User ID :$userId")
//        RetrofitClient.instance.getUserdata(userId)
//                .enqueue(object : Callback<User> {
//                    override fun onFailure(call: Call<User>, t: Throwable) {
//
//                        Toast.makeText(applicationContext, "Progress Page has Failed!", Toast.LENGTH_SHORT).show()
//                        d("LoginPage", "Get user data failed!")
//                    }
//
//                    override fun onResponse(call: Call<User>, response: Response<User>) {
//                       //d("LoginPage", "user firstname : ${response.body()!!.first_name}")
//                        //start MainActivity
//                        val intent = Intent(applicationContext, MainActivity::class.java)
//                        UserSingleton.customerDataSingleton = response.body()!!
//                        UserSingleton.usernameSingleton=editText_username.text.toString()
//                        d("LoginPage", "DATA : ${response.body()!!}")
//                        d("LoginPage", "Singleton Data : ${UserSingleton.customerDataSingleton!!}")
//                        startActivity(intent)
//                        finish()
//                    }
//
//                })

                RetrofitClient.instance.getConfigs(userId)
                .enqueue(object : Callback<ConfigsResponse> {
                    override fun onFailure(call: Call<ConfigsResponse>, t: Throwable) {

                        Toast.makeText(applicationContext, "Progress Page has Failed!", Toast.LENGTH_SHORT).show()
                        d("LoginPage", "Get user data failed!")
                    }

                    override fun onResponse(call: Call<ConfigsResponse>, response: Response<ConfigsResponse>) {
                       //d("LoginPage", "user firstname : ${response.body()!!.first_name}")
                        //start MainActivity
                        val intent = Intent(applicationContext, MainActivity::class.java)
                        Singleton.config = response.body()!!
                        Singleton.usernameSingleton=editText_username.text.toString()
                        d("LoginPage", "configSingleton : ${Singleton.config}")
                        d("LoginPage", "Username Data : ${Singleton.usernameSingleton!!}")
                        startActivity(intent)
                        finish()
                    }

                })
    }
}
