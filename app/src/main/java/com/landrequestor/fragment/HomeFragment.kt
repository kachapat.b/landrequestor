package com.landrequestor.fragment

import android.app.Dialog
import android.content.Intent
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.os.Handler
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.landrequestor.R
import com.landrequestor.ViewDeedsData
import com.landrequestor.api.RetrofitClient
import com.landrequestor.model.Singleton
import com.landrequestor.model.configs.Status
import com.landrequestor.model.deed.All_Deeds
import kotlinx.android.synthetic.main.filter.view.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.list_filter.view.*
import kotlinx.android.synthetic.main.list_style.view.*
import kotlinx.android.synthetic.main.progress_dialog.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class HomeFragment : Fragment() {
    lateinit var deeds : All_Deeds
    val configData = Singleton.config
    var configStatus = configData?.resources?.status
    var status=ArrayList<Status>()
    var user_id = configData?.user?.user_id
    var status_id : String?="not:5"
    lateinit var dialog: Dialog
    lateinit var mdialog: Dialog


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        status.add(Status("All",null))
        configStatus?.forEach {
            status.add(it)
        }


        swipe_refresh_layout.setOnRefreshListener {
            Handler().postDelayed({
            loadData(user_id,status_id)
            swipe_refresh_layout.isRefreshing = false
            }, 1000)
        }
        swipe_refresh_layout.setColorSchemeColors(resources.getColor(R.color.darkBlue),resources.getColor(R.color.lightBlue))


        filter_btn.setOnClickListener {

            val mBuilder= AlertDialog.Builder(requireContext())
            var mDialogView =layoutInflater.inflate(R.layout.filter,null)
            mBuilder.setView(mDialogView)
            mBuilder.setCancelable(true)

            mDialogView.filter_recycleview.adapter=FilterAdapter()
            mDialogView.filter_recycleview.layoutManager= LinearLayoutManager(context)

            mdialog=mBuilder.create()
            mdialog.show()

       }
        loadData(user_id,status_id)

    }

private fun loadData(customer_ID: Int?,status_Id:String?=status_id) {
    loadingDialog()
    RetrofitClient.instance.getAllDeed(customer_ID,status_Id!!)
        .enqueue(object : Callback<All_Deeds> {
            override fun onFailure(call: Call<All_Deeds>, t: Throwable) {
                d("HomePage", "Call Request Data Failed! ${t.message}")
            }

            override fun onResponse(call: Call<All_Deeds>, response: Response<All_Deeds>) {
                if(response.body()!=null){
                    deeds = response.body()!!
                    d("HomePage", "Show Deeds List Data : $deeds")

                        deeds_list.visibility = View.VISIBLE
                        deeds_list.adapter = HomeAdapter()
                        deeds_list.layoutManager = LinearLayoutManager(context)
                        cnt_req_txt.text = deeds.size.toString() + " Requests"
                }
                else{
                    deeds_list.visibility=View.GONE
                    cnt_req_txt.text="0 Requests"
                }

                dialog.dismiss()


            }
        })

}

    inner class HomeAdapter : RecyclerView.Adapter<HomeAdapter.HomeHolder>() {
        override fun onCreateViewHolder(parentView:  ViewGroup, option: Int): HomeHolder {
            val view=LayoutInflater.from(context).inflate(R.layout.list_style,parentView,false)
            return HomeHolder(view)
        }

        inner  class HomeHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var Row=itemView.row
            var DeedsName=itemView.txt_deed_name
            var Status=itemView.txt_status
            var Address=itemView.txt_address
            var Date=itemView.txt_date
            var Status_color=itemView.status_color
        }

        override fun getItemCount(): Int { return deeds.size }

        override fun onBindViewHolder(holder: HomeHolder, position: Int) {
            holder.DeedsName.text=deeds[position].deedName
            var status_name = status.find { it.statusId == deeds[position].statusId }?.name
            holder.Status.text=status_name
            holder.Address.text=deeds[position].address

            val status = holder.Status_color.background.mutate() as GradientDrawable
            var x=deeds[position].statusId
            when(x) {
                4 -> status.setColor(resources.getColor(R.color.success))
                1 -> status.setColor(resources.getColor(R.color.waiting))
                2 -> status.setColor(resources.getColor(R.color.assign))
                3 -> status.setColor(resources.getColor(R.color.collecting))
                else -> status.setColor(resources.getColor(R.color.Red))
            }

            holder.Date.text=deeds[position].createdDate
//            if(deeds[position].updatedDate!=null){ holder.Date.text=deeds[position].updatedDate }
//            else{ holder.Date.text=deeds[position].createdDate }

            holder.Row.setOnClickListener {
                val intent = Intent(context, ViewDeedsData::class.java)
                d("HomePage", "Req data Row: ${deeds[position]}")
                intent.putExtra("DeedID", deeds[position].deedId)
                startActivity(intent)
            }

        }

    }
    fun loadingDialog() {
        val builder= AlertDialog.Builder(requireContext())
        val dialogView=layoutInflater.inflate(R.layout.progress_dialog,null)
        dialogView.dialog_txt.text="Loading..."
        builder.setView(dialogView)
        builder.setCancelable(false)
        dialog=builder.create()
        dialog.show()
    }

    inner class FilterAdapter : RecyclerView.Adapter<FilterHolder>() {
        override fun onCreateViewHolder(parentView: ViewGroup, option: Int): FilterHolder {
            val view= LayoutInflater.from(requireContext()).inflate(R.layout.list_filter,parentView,false)
            return FilterHolder(view)
        }

        override fun getItemCount(): Int { return status.size-1 }

        override fun onBindViewHolder(holder: FilterHolder, position: Int) {

                holder.filter_name.text= status[position].name
                holder.row.setOnClickListener {
                    filter_btn.text=status[position].name
                    if(status[position].name?.toUpperCase()=="ALL"){
                        loadData(user_id, "not:5")
                        status_id="not:5"
                    }else{
                        loadData(user_id, status[position].statusId.toString())
                        status_id=status[position].statusId.toString()
                    }

                    mdialog.dismiss()
            }

        }
    }
    inner  class FilterHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var row=itemView.row_filter_list
        var filter_name=itemView.txt_filter_list

    }


}
