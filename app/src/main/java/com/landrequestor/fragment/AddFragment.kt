package com.landrequestor.fragment

import android.app.Activity
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import com.esri.arcgisruntime.ArcGISRuntimeEnvironment
import com.esri.arcgisruntime.geometry.Point
import com.esri.arcgisruntime.geometry.SpatialReferences
import com.esri.arcgisruntime.mapping.ArcGISMap
import com.esri.arcgisruntime.mapping.Basemap
import com.esri.arcgisruntime.mapping.view.Graphic
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay
import com.esri.arcgisruntime.symbology.PictureMarkerSymbol
import com.esri.arcgisruntime.symbology.SimpleMarkerSymbol
import com.landrequestor.MainActivity
import com.landrequestor.PinMap
import com.landrequestor.R
import com.landrequestor.api.RetrofitClient
import com.landrequestor.helper.FileUtil.from
import com.landrequestor.model.LatLon
import com.landrequestor.model.Singleton
import com.landrequestor.model.Singleton.config
import kotlinx.android.synthetic.main.fragment_add.*
import kotlinx.android.synthetic.main.image_show_style.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class AddFragment : Fragment() {
    var customerId : Int? = null
    var customerData = config?.user

    var PICK_IMAGE_MULTIPLE = 1
    var imagesEncodedList: MutableList<File?> ?= null

    private var fileUri: Uri? = null
    private var imgList : ArrayList<MultipartBody.Part>?= ArrayList()

    var lat:Double?=null
    var lon:Double?=null

    lateinit var pointGraphic: Graphic
    lateinit var point: Point
    //Mark the location
    private var mGraphicsOverlay: GraphicsOverlay? = null
    private var mPointSymbol: SimpleMarkerSymbol? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_add, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_select_map.visibility=View.INVISIBLE
        customerId = customerData?.customer_id
//        d("AddPage","Customer ID : $customerId")

        setupMap()

        if(arguments != null){
            lat = arguments!!.getDouble("latArg")
            lon = arguments!!.getDouble("lonArg")
        }else{
            lat= LatLon.latitude
            lon=LatLon.longitude
        }

        if (lat!=null && lon!=null){
            setupMap(17,lat,lon)
            val mDrawable = ContextCompat.getDrawable(context!!, R.drawable.pin) as BitmapDrawable
            createPointGraphics(lat,lon,mDrawable)
        }

        if(lat==null || lon==null){
            btn_select_map.visibility=View.VISIBLE
            layout_DeedName.isEnabled=false
            layout_Address.isEnabled=false
            Landmark.isEnabled=false
            btn_upload_img.isEnabled=false
//            (Toast.makeText(requireContext(),"First step, select your location",Toast.LENGTH_SHORT).show())
        }

        txt_location.text="latitude : ${String.format("%.6f", lat)}, longitude : ${String.format("%.6f", lon)}"


        btn_select_map.setOnClickListener {
            val intent = Intent(context, PinMap::class.java)
            startActivity(intent)
        }

        btn_submit.setOnClickListener {
            if(txt_DeedName.text?.trim().toString()==""){layout_DeedName.error="Deed name required"}
            if(txt_Address.text?.trim().toString()==""){layout_Address.error="Address required"}
            if(lat==null || lon==null){ Toast.makeText(activity!!,"Select the location.",Toast.LENGTH_SHORT).show()}
            if(txt_DeedName.text?.trim().toString()!="" && txt_Address.text?.trim().toString()!="" && lat!=null && lon!=null) {uploadMultipartFile()}//+Data

        }

        //Upload Image
        btn_upload_img.setOnClickListener{
//            if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) !== PackageManager.PERMISSION_GRANTED) {
//                ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), 0)
//            }
            pick()
        }

    }

    fun setupMap(levelmap : Int=1, Lat : Double?=13.702839, Lon : Double?=100.543718) {
        ArcGISRuntimeEnvironment.setLicense(resources.getString(R.string.arcgis_license_key))
            val basemapType = Basemap.Type.OPEN_STREET_MAP
            show_map.isAttributionTextVisible = false
            show_map.map = ArcGISMap(basemapType, Lat!!, Lon!!, levelmap)
            //Add layer mark location
            mGraphicsOverlay = GraphicsOverlay()
            show_map.graphicsOverlays.add(mGraphicsOverlay)

    }

    fun createPointGraphics(Lat : Double?, Lon : Double?, mSymbol:BitmapDrawable?) {
        if (Lat != null && Lon != null) {
            mGraphicsOverlay?.graphics?.clear()

            val pinSymbol: PictureMarkerSymbol = PictureMarkerSymbol.createAsync(mSymbol).get()
            pinSymbol.height = 30F
            pinSymbol.width = 30F
            pinSymbol.loadAsync()

            mPointSymbol = SimpleMarkerSymbol(SimpleMarkerSymbol.Style.X, -0xffc757, 20F)

            point = Point(Lon, Lat, SpatialReferences.getWgs84())
            pointGraphic = Graphic(point, pinSymbol)
            mGraphicsOverlay?.graphics?.add(pointGraphic)
        }
    }

    private fun uploadMultipartFile() {
        //Data
        // val x = RequestBody.create(MediaType.parse("text/form-data"), "text")
        val id = RequestBody.create(MultipartBody.FORM, customerId.toString())
        val deed_name = RequestBody.create(MultipartBody.FORM, txt_DeedName.text.toString())
        val landmark = RequestBody.create(MultipartBody.FORM, txt_Landmark.text.toString())
        val address = RequestBody.create(MultipartBody.FORM, txt_Address.text.toString())
        val latitude = RequestBody.create(MultipartBody.FORM, lat.toString())
        val longitude = RequestBody.create(MultipartBody.FORM, lon.toString())

        d("AddPage", "imagesEncodedList before upload : $imagesEncodedList")
        imagesEncodedList?.forEach {
            val imgname = (Calendar.getInstance().timeInMillis).toString()
            val requestBody = RequestBody.create(MediaType.parse(config!!.allowedImageFormats!![0]), it) //("image/jpg")
            val fileToUpload = MultipartBody.Part.createFormData("image", it?.name, requestBody)
            imgList!!.add(fileToUpload)
        }
            d("AddPage","lat $lat, lon $lon")
            val builder = AlertDialog.Builder(activity!!)
            builder.setTitle("Confiirm?")
            builder.setMessage("Are you sure to add this data?")
            builder.setPositiveButton("YES") { dialog, which ->
                RetrofitClient.instance.addRequest(id,deed_name,address,landmark,latitude,longitude,imgList!!)
                        .enqueue(object: Callback<ResponseBody> {
                            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                                Toast.makeText(activity!!, "Add data Fail!", Toast.LENGTH_SHORT).show()
                            }
                            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                                Toast.makeText(activity!!, "Add Complete.", Toast.LENGTH_SHORT).show()
                                //Clear Data
                                txt_DeedName.text?.clear()
                                txt_Address.text?.clear()
                                txt_Landmark.text?.clear()
                                imgList=null
                                lat=null
                                lon=null
                                LatLon.longitude=null
                                LatLon.latitude=null

                                val transaction = fragmentManager!!.beginTransaction()
                                transaction.replace(R.id.fragment_layout,HomeFragment())
                                transaction.addToBackStack(null)
                                transaction.commit()

                            }
                        })
            }
            builder.setNegativeButton("No"){dialog,which ->
                Toast.makeText(activity!!,"You aren't agree.",Toast.LENGTH_SHORT).show()
            }
            val dialog: AlertDialog = builder.create()
            dialog.show()

    }


    private fun pick(){
        MaterialDialog.Builder(requireContext()).title("Upload your image").items(R.array.uploadImg).itemsIds(R.array.itemId).itemsCallback { dialog, view, which, text ->
            when (which) {
                0 -> {
//                        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
//                        startActivityForResult(galleryIntent, REQUEST_PICK_PHOTO)
                    val pickImg = Intent()
                    pickImg.type = "image/*"
                    pickImg.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
                    pickImg.action = Intent.ACTION_GET_CONTENT
                    startActivityForResult(Intent.createChooser(pickImg, "Select Picture"), PICK_IMAGE_MULTIPLE)
                }
                1 -> {
                    show_img.visibility=View.GONE
                    imagesEncodedList=null
                }
            }
        }
            .show()
    }

    companion object {
        private val REQUEST_TAKE_PHOTO = 0
        private val REQUEST_PICK_PHOTO = 2
        private val CAMERA_PIC_REQUEST = 1111

        private val TAG = MainActivity::class.java.simpleName

        private val CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100

        val MEDIA_TYPE_IMAGE = 1
        val IMAGE_DIRECTORY_NAME = "Android File Upload"

        private fun getOutputMediaFile(type: Int): File? {
            // External sdcard location
            val mediaStorageDir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME)
            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    d(TAG, "Oops! Failed create " + IMAGE_DIRECTORY_NAME + " directory")
                    return null
                }
            }

            // Create a media file name
            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(Date())
            val mediaFile: File
            if (type == MEDIA_TYPE_IMAGE) {
                mediaFile = File(mediaStorageDir.path + File.separator + "IMG_" + ".jpg")
            } else {
                return null
            }
            return mediaFile
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO || requestCode == REQUEST_PICK_PHOTO || requestCode == PICK_IMAGE_MULTIPLE ) {

                imagesEncodedList=ArrayList() //List Of Image[File Type]

                //Add 1 img
                if (data?.data != null) {
                    // Get the Image from data
                    val selectedImage = data.data
                    var fileitem=from(requireContext(),selectedImage)
                    imagesEncodedList?.add(fileitem)

                }else{//Add >2 img
                    if (data?.clipData != null){
                        val mClipData = data.clipData
                        for (i in 0 until mClipData!!.itemCount) {
                            val item = mClipData.getItemAt(i)
                            val uri = item.uri
                            var fileitem=from(requireContext(),uri)
                            imagesEncodedList?.add(fileitem)
                        }
                    }
                }
                }
                d("AddPage","imagesEncodedList : $imagesEncodedList")
//                imageView.visibility=View.VISIBLE
//                imagesEncodedList!!.forEach {
//                    val myBitmap = BitmapFactory.decodeFile(it!!.absolutePath)
//                    imageView.setImageBitmap(myBitmap)
//                }
                if(imagesEncodedList!=null){
                    if(imagesEncodedList!!.size<=config?.maximumFileImage!!){
                    show_img.visibility=View.VISIBLE
                    show_img.adapter = ImgShowAdapter()
                    show_img.layoutManager= LinearLayoutManager(requireContext(),LinearLayoutManager.HORIZONTAL, false)
                    }else{
                        val builder = AlertDialog.Builder(requireContext())
                        builder.setTitle("Message")
                        builder.setMessage("Select a maximum of ${config?.maximumFileImage!!} images.")
                        builder.setPositiveButton("DONE") { dialog, id -> }
                        builder.show()
                        imagesEncodedList=null
                    }
            }
        } else { Toast.makeText(requireContext(), "Sorry, there was an error!", Toast.LENGTH_LONG).show()}
    }
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putParcelable("file_uri", fileUri)
    }

    //Show imagr preview
    inner class ImgShowAdapter : RecyclerView.Adapter<ImgShowHolder>() {
        override fun onCreateViewHolder(parentView: ViewGroup, option: Int): ImgShowHolder {

            val view= LayoutInflater.from(requireContext()).inflate(R.layout.image_show_style,parentView,false)
            return ImgShowHolder(view)
        }

        override fun getItemCount(): Int { return imagesEncodedList!!.size}

        override fun onBindViewHolder(holder: ImgShowHolder, position: Int) {
            // Set the Image in ImageView for Previewing the Media
            val myBitmap = BitmapFactory.decodeFile(imagesEncodedList!![position]!!.absolutePath)
            holder.img.setImageBitmap(myBitmap)


        }
    }
    inner  class ImgShowHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val img=itemView.imgshowstyle


    }

}
