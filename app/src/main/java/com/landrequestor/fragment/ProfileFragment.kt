package com.landrequestor.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.landrequestor.Login

import com.landrequestor.R
import com.landrequestor.model.CheckFragment
import com.landrequestor.model.LatLon
import com.landrequestor.model.Singleton
import kotlinx.android.synthetic.main.fragment_profile.*

/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : Fragment() {

    var username = Singleton
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        profile_firstname.setText(username.config?.user?.first_name)
        profile_lastname.setText(username.config?.user?.last_name)
        profile_phone.setText(username.config?.user?.phone)
        profile_username.setText(username.usernameSingleton)


        btn_logout.setOnClickListener(){
            val builder = AlertDialog.Builder(activity!!)
            builder.setTitle("Confiirm?")
            builder.setMessage("Are you sure to logout?")
            builder.setPositiveButton("YES") { dialog, which ->
                Singleton.config=null
                Singleton.usernameSingleton=null
                LatLon.latitude=null
                LatLon.longitude=null
                CheckFragment.frg=null

                val i = Intent(context, Login::class.java)
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(i)
                activity!!.finish()
            }
            builder.setNegativeButton("No"){dialog,which ->
                Toast.makeText(activity!!,"You aren't agree.", Toast.LENGTH_SHORT).show()
            }
            val dialog: AlertDialog = builder.create()
            dialog.show()




        }
    }

}
