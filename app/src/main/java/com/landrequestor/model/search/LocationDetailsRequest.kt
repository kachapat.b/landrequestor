package com.landrequestor.model.search

data class LocationDetailsRequest (
    var locationid: String?,
    var key: String?
)