package com.landrequestor.model.search

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AutocompleteRequest (
    var keyword : String?=null,
    var key : String?=null,
    var maxResult : Int?=null
): Parcelable
