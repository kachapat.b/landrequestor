package com.landrequestor.model.search


import com.google.gson.annotations.SerializedName

data class LocationDetails(@SerializedName("data") var `data`: Data?)

data class Data(
    @SerializedName("BusinessName")
    var businessName: Any?,
    @SerializedName("District")
    var district: String?,
    @SerializedName("DistrictPrefix")
    var districtPrefix: String?,
    @SerializedName("FormattedAddress")
    var formattedAddress: String?,
    @SerializedName("HouseNumber")
    var houseNumber: String?,
    @SerializedName("LAT_LON")
    var LATLON: String?,
    @SerializedName("LanguageCode")
    var languageCode: String?,
    @SerializedName("LocationID")
    var locationID: String?,
    @SerializedName("Moo")
    var moo: Any?,
    @SerializedName("PostalCode")
    var postalCode: String?,
    @SerializedName("PremiseLaneName")
    var premiseLaneName: Any?,
    @SerializedName("PremiseName")
    var premiseName: String?,
    @SerializedName("Province")
    var province: String?,
    @SerializedName("ProvincePrefix")
    var provincePrefix: Any?,
    @SerializedName("StreetFullName")
    var streetFullName: String?,
    @SerializedName("StreetLeadingType")
    var streetLeadingType: String?,
    @SerializedName("StreetName")
    var streetName: String?,
    @SerializedName("StreetTrailingType")
    var streetTrailingType: Any?,
    @SerializedName("SubDistrict")
    var subDistrict: String?,
    @SerializedName("SubDistrictPrefix")
    var subDistrictPrefix: String?,
    @SerializedName("SubStreetLeadingType")
    var subStreetLeadingType: Any?,
    @SerializedName("SubStreetName")
    var subStreetName: Any?,
    @SerializedName("SubStreetTrailingType")
    var subStreetTrailingType: Any?
)