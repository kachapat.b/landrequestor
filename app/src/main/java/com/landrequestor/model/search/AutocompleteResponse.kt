package com.landrequestor.model.search

data class ResultOfAutocomplete (var LocationID:String ?=null, var FormattedAddress : String?=null)
data class Autocomplete(var data : List<ResultOfAutocomplete>)