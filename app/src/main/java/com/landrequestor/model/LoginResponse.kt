package com.landrequestor.model

data class LoginResponse (
    val message: String?= null,
    val user_id: Int?= null
)