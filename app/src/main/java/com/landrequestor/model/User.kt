package com.landrequestor.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import org.parceler.Parcel

@Parcelize
data class User (
    var customer_id : Int? = null,
    var user_id : Int? = null,

    var first_name : String? = null,
    var last_name : String? = null,
    @SerializedName("phone_number")
    var phone: String? = null


): Parcelable