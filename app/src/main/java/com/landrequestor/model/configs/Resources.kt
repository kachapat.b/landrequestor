package com.landrequestor.model.configs


import com.google.gson.annotations.SerializedName

data class Resources(
    @SerializedName("deeds")
    var deeds: Deeds?,
    @SerializedName("status")
    var status: List<Status>?
)