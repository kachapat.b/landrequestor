package com.landrequestor.model.configs


import com.google.gson.annotations.SerializedName
import com.landrequestor.model.User

data class ConfigsResponse(
    @SerializedName("allowed_image_formats")
    var allowedImageFormats: List<String>?,
    @SerializedName("app_name")
    var appName: String?,
    @SerializedName("maximum_file_image")
    var maximumFileImage: Int?,
    @SerializedName("maximum_image_size")
    var maximumImageSize: Int?,
    @SerializedName("resources")
    var resources: Resources?,
    @SerializedName("user")
    var user: User?
)