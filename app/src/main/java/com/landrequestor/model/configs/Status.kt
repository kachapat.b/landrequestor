package com.landrequestor.model.configs


import com.google.gson.annotations.SerializedName

data class Status(
    @SerializedName("name")
    var name: String?,
    @SerializedName("status_id")
    var statusId: Int?
)