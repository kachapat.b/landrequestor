package com.landrequestor.model.configs


import com.google.gson.annotations.SerializedName

data class Deeds(
    @SerializedName("url")
    var url: String?
)