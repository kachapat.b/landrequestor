package com.landrequestor.model

data class CancelResponse (
        val message: String?= null
    )