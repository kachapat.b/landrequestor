package com.landrequestor.model


import com.google.gson.annotations.SerializedName

data class CancelRequest(
    @SerializedName("status_id")
    var status_id: Int?
)