package com.landrequestor.model.deed


import com.google.gson.annotations.SerializedName

data class All_DeedsItem(
    @SerializedName("address")
    var address: String?,
    @SerializedName("area")
    var area: Double?,
    @SerializedName("created_date")
    var createdDate: String?,
    @SerializedName("customer")
    var customer: Customer?,
    @SerializedName("deed_id")
    var deedId: Int?,
    @SerializedName("deed_name")
    var deedName: String?,
    @SerializedName("deed_number")
    var deedNumber: String?,
    @SerializedName("deed_type")
    var deedType: DeedType?,
    @SerializedName("employee")
    var employee: Employee?,
    @SerializedName("image_path")
    var imagePath: List<String>?,
    @SerializedName("latitude")
    var latitude: Double?,
    @SerializedName("longitude")
    var longitude: Double?,
    @SerializedName("nearby_landmark")
    var nearbyLandmark: String?,
    @SerializedName("status_id")
    var statusId: Int?,
    @SerializedName("updated_date")
    var updatedDate: String?
)