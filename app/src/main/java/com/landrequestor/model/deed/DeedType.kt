package com.landrequestor.model.deed


import com.google.gson.annotations.SerializedName

data class DeedType(
    @SerializedName("deed_type_id")
    var deedTypeId: Int?,
    @SerializedName("name")
    var name: String?
)