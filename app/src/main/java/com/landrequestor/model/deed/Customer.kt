package com.landrequestor.model.deed


import com.google.gson.annotations.SerializedName

data class Customer(
    @SerializedName("customer_id")
    var customerId: Int?,
    @SerializedName("first_name")
    var firstName: String?,
    @SerializedName("last_name")
    var lastName: String?,
    @SerializedName("phone_number")
    var phoneNumber: String?,
    @SerializedName("user_id")
    var userId: Int?
)