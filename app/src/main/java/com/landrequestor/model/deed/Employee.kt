package com.landrequestor.model.deed


import com.google.gson.annotations.SerializedName

data class Employee(
    @SerializedName("employee_id")
    var employeeId: Int?,
    @SerializedName("first_name")
    var firstName: String?,
    @SerializedName("last_name")
    var lastName: String?,
    @SerializedName("user_id")
    var userId: Int?
)