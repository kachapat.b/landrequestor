package com.landrequestor.model


import com.google.gson.annotations.SerializedName


//class AllDeeds : ArrayList<Deed>()
//data class Deed(
//    @SerializedName("address")
//    var address: String?,
//    @SerializedName("nearby_landmark")
//    var landmark: String?,
//    @SerializedName("area")
//    var area: Double?,
//    @SerializedName("created_date")
//    var createdDate: String?,
//    @SerializedName("customer")
//    var customer: Customer?,
//    @SerializedName("deed_id")
//    var deedId: Int?,
//    @SerializedName("deed_name")
//    var deedName: String?,
//    @SerializedName("deed_number")
//    var deedNumber: String?,
//    @SerializedName("deed_type")
//    var deedType: DeedType?,
//    @SerializedName("employee")
//    var employee: Employee?,
//    @SerializedName("image_path")
//    var imagePath: List<String>?,
//    @SerializedName("latitude")
//    var latitude: Double?,
//    @SerializedName("longitude")
//    var longitude: Double?,
//    @SerializedName("status")
//    var status: Status?,
//    @SerializedName("updated_date")
//    var updatedDate: String?
//)
//
//data class DeedType(
//    @SerializedName("deed_type_id")
//    var deedTypeId: Int?,
//    @SerializedName("name")
//    var name: String?
//)
//
//data class Customer(
//    @SerializedName("customer_id")
//    var customerId: Int?,
//    @SerializedName("first_name")
//    var firstName: String?,
//    @SerializedName("last_name")
//    var lastName: String?,
//    @SerializedName("phone_number")
//    var phoneNumber: String?,
//    @SerializedName("user_id")
//    var userId: Int?
//)
//
//data class Employee(
//    @SerializedName("employee_id")
//    var employeeId: Int?,
//    @SerializedName("first_name")
//    var firstName: String?,
//    @SerializedName("last_name")
//    var lastName: String?,
//    @SerializedName("user_id")
//    var userId: Int?
//)
//
//data class Status(
//    @SerializedName("name")
//    var name: String?,
//    @SerializedName("status_id")
//    var statusId: Int?
//)