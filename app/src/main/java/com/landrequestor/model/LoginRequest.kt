package com.landrequestor.model

data class LoginRequest (
        val username: String,
        val password: String,
        val role: String
)