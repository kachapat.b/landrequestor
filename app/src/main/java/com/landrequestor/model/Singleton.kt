package com.landrequestor.model

import com.landrequestor.model.configs.ConfigsResponse

object Singleton {
//    var customerDataSingleton: User? = null
    var usernameSingleton: String? = null
    var config:ConfigsResponse?=null
}