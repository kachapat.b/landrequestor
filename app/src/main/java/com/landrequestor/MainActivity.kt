package com.landrequestor

import android.app.Dialog
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.component1
import androidx.core.view.get
import androidx.fragment.app.Fragment
import com.landrequestor.fragment.AddFragment
import com.landrequestor.fragment.HomeFragment
import com.landrequestor.fragment.ProfileFragment
import com.landrequestor.model.CheckFragment
import com.landrequestor.model.LatLon
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.progress_dialog.view.*


class MainActivity : AppCompatActivity() {
    lateinit var dialog:Dialog


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        var values = intent.getBooleanExtra("checkFrg",false)

        if(CheckFragment.frg=="add"){

            loadFragment(AddFragment())
            bottom_nav.selectedItemId=R.id.add
            CheckFragment.frg="home"
        }
        else{
//            loadingDialog()

            loadFragment(HomeFragment())
            bottom_nav.selectedItemId=R.id.home
        }

        bottom_nav.setOnNavigationItemSelectedListener { item ->

            when (item.itemId) {
                R.id.home -> {
//                    loadingDialog()
                    item.isEnabled=false
                    bottom_nav.menu.findItem(R.id.profile).isEnabled=true
                    bottom_nav.menu.findItem(R.id.add).isEnabled=true

                    LatLon.latitude=null
                    LatLon.longitude=null
                    loadFragment(HomeFragment())

                    return@setOnNavigationItemSelectedListener true
                }
                R.id.add -> {
                    item.isEnabled=false
                    bottom_nav.menu.findItem(R.id.profile).isEnabled=true
                    bottom_nav.menu.findItem(R.id.home).isEnabled=true

                    loadFragment(AddFragment())
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.profile -> {
                    item.isEnabled=false
                    bottom_nav.menu.findItem(R.id.home).isEnabled=true
                    bottom_nav.menu.findItem(R.id.add).isEnabled=true

                    LatLon.latitude=null
                    LatLon.longitude=null
                    loadFragment(ProfileFragment())
                    return@setOnNavigationItemSelectedListener true
                }
                else -> {
                    return@setOnNavigationItemSelectedListener  false
                }
            }
        }

    }

    private fun loadFragment(fragment:Fragment){
        supportFragmentManager.beginTransaction().also { fragmentTransaction ->
            fragmentTransaction.replace(R.id.fragment_layout,fragment)
            fragmentTransaction.commit()
        }
    }

    fun loadingDialog() {
        val builder= AlertDialog.Builder(this)
        val dialogView=layoutInflater.inflate(com.landrequestor.R.layout.progress_dialog,null)
        dialogView.dialog_txt.text="Loading..."
        builder.setView(dialogView)
        builder.setCancelable(false)
        dialog=builder.create()
        dialog.show()
        Handler().postDelayed({dialog.dismiss()},1200)
    }
}
